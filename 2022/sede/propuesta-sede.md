---
layout: 2022/default
section: sede
permalink: /2022/propuesta-sede
title: Propón sede para esLibre 2022
description: Propón sede para esLibre 2022
---

<h1 class="mt-3">
  <strong>Propón sede para es<span class="red">Libre</span> 2022</strong>
</h1>

Cuando un año termina otro empieza, así que eso significa que llega el momento de comenzar a organizar otra edición de **es<span class="red">Libre</span>**. Desde la primera edición en Granada han pasado ya más de 2 años y 3 ediciones, habiendo llegado cada vez a más personas y aumentando el número de propuestas recibidas edición tras edición, queda plantearse qué toca hacer ahora.  

**es<span class="red">Libre</span> es un encuentro de personas interesadas en las tecnologías libres y cultura libre en general enfocado a compartir conocimiento y experiencia alrededor de las mismas**, por eso nos gustaría intentar acercarlo a todas las regiones de la península (y quien sabe si en alguna ocasión también a las islas). Precisamente para ese objetivo necesitamos contar con la colaboración de comunidades o grupos locales que nos ayuden en esta labor con la idea de realizar una organización compartida: por ello ahora te pedimos que nos ayudes a organizar **es<span class="red">Libre</span> 2022**.  

### Equipo organizador

En **es<span class="red">Libre</span>** no existe una _cúpula_ dirigente como tal, pero sí que existe un grupo organizador _general_ que son las personas que estuvieron relacionadas con la organización de la pasada edición y que han presentado su interés en seguir participando activamente en las cuestiones organizativas. **[LibreLabGRX](https://librelabgrx.cc), [OfiLibre URJC](https://ofilibre.gitlab.io/) o [LibreLabUCM](https://librelabucm.org/)** se han encargado de esta tarea en pasadas ediciones (contando con la ayuda de otros grupos como **[Interferencias](https://interferencias.tech/), [Wikimedia España](https://www.wikimedia.es/) o [GALPon](https://www.galpon.org/) entre otros**).

La idea es que aunque las **cuestiones organizativas y la producción del evento en general sea una tarea compartida** con todas las personas que se mueven con el interés de darle forma a esta comunidad, **haya un grupo o comunidad anfitriona local en cada edición que facilite el realizar el evento en diferentes zonas geográficas** para cada una de las mismas, dando así la posibilidad de tejer redes entre personas y comunidades en espacios comunes que quizás no tengan un contacto directo por pertenecer a ámbitos culturales, profesionales o regionales distantes entre si.

Para que el proceso sea tan transparente como el resto de decisiones relacionados con el evento, todas las propuestas serán públicas y accesibles en todo momento desde [este repositorio](https://gitlab.com/eslibre/coord/-/tree/master/propuestas/2022). Por eso mismo agradeceríamos a todas las propuestas recibidas con expliquen resumidamente quienes son, si tienen experiencia previa en la organización de actividades de esta misma temática y un poco la motivación a la hora de organizar un evento de este tipo.

### Formato del congreso

**El ser la cuarta edición hace que tengamos ya un tipo de esquema base bien definido para el congreso, pero igualmente abierto a cualquier tipo de propuesta que ayude a que sea más participativo e inclusivo**. El esquema base que se ha venido desarrollando sería algo del estilo:

-   Sesiones plenarias de apertura y cierre del congreso con todos los asistentes, participantes y organizadores
-   Una serie de pistas (que podrían ser temáticas o generales) en las que se llevarían a cabo las propuestas de actividad recibidas
-   Salas o espacios abiertos organizadas por diferentes comunidades, comprometiéndose estas a coordinarse con la organización local para intentar que logísticamente todas las actividades sean lo más homogéneas posibles (pero comprendiendo que en todo momento se respetara su criterio para las mismas)
-   Actividades sociales que también ayuden al intercambio de ideas y la socialización en entorno más distendido
-   Se puede tomar como ejemplo [el programa final de la última edición](/2021/programa/) para hacerse una idea

También después de dos ediciones virtuales y solo una presencial, nos gustaría poder volver a este última modalidad. Aunque es una gran ventaja poder conectarse desde cualquier sitio para seguir el congreso, y esto nos ha permitido contar con la participación de personas de varios países e incluso otros continentes, hay ciertos momentos en los que las ideas se transmiten mejor con la espontaneidad del mundo físico.

**Lo ideal sería contar con un plan para realizar el congreso siguiendo algún tipo de modalidad mixta, donde aunque el evento fuera principalmente presencial, se dispusiera de la posibilidad de seguirlo o participar de forma remota** en determinadas situaciones, como por ejemplo, el de personas que no puedan permitirse el desplazarse hasta el lugar del congreso o que directamente les es inviable por la dificultad que representa debido a la distancia.

Además, el partir de un modelo mixto también tendrían en cuenta que dada la situación actual, se tendrían margen de maniobra en el, deseamos que improbable caso, de que situación sanitaria actual cambiase y la realización presencial del evento no fuera necesario. Contar con las experiencias de años anteriores sin duda es algo que facilitará esta tarea.

### Acciones para mejorar la diversidad en la participación

**La organización [se compromete](/conducta) a crear un ambiente seguro, no tóxico y diverso**, de modo que nos encantaría dar voz a ponentes de las varias comunidades minoritarias que hagan uso o desarrollen software, hardware o cultura libre. Somos conscientes de que hay muchas personas que usan y crean tecnologías libre y apoyan activamente la divulgación de la cultura libre, y muchas veces no son escuchadas, queremos cambiar eso: ofrecemos nuestro micrófono y espacio para que se hable desde estas comunidades.

Desde la organización queremos aprender y crear puentes, para que la tecnología sea (al fin) un herramientas de apoyo y colaboración: buscamos un futuro colaborativo, multidisciplinar y libre.

También buscamos que las diferentes comunidades nos hagan saber lo que quieran contar desde el desarrollo o la experiencia usando software libre, por eso también nos gustaría conocer iniciativas en esta línea de acción que podrían proponer desde el equipo de organización local. Algunas de estas líneas de las que trabajamos desde la organización general:

- Establecer mecanismos activos a los que recurrir fácilmente para asegurarse de que el código de conducta se cumple rápidamente ante cualquier comportamiento alertado que lo incumpla sin excepciones ni para ponentes, organización, asistentes o participantes de <strong>es<span class="red">Libre</span></strong>.
- La diversidad no debe centrarse únicamente en el género, por lo que fomentar distintos tipos de participación que intenten disminuir el sentimiento de exclusión que puedan sentir las personas debido a su origen étnico, religión, identidad de género, edad, habilidades físicas y experiencia u otras características de su identidad.
- Comprometerse a facilitar la participación de personas que tengan responsabilidades de cuidados, algo que aunque puede ser una barrera para la asistencia y envío de propuestas tanto para hombres como para mujeres, la responsabilidad a menudo recae en gran medida en las mujeres.
- Realizar una comunicación activamente diversa e inclusiva mediante diferentes canales de comunicación, intentando que todo el material proporcionado por la organización esté escrito en fuentes y colores que sean más fáciles de leer para las personas con dislexia u otras dificultades de lectura/aprendizaje, o garantizar un tamaño de fuente y un color de fuente suficientemente grande que contraste con el fondo, para ayudar a las personas con problemas visuales.

### Posibles fechas

Aunque no hay nunca hay fechas perfectas, quizás los meses de **mayo-junio-julio** son la mejor opción (además que el clima suele ser también más agradable en esos meses 🙂), al igual que también tener la posibilidad de que **al menos una de los días del evento caiga en fin de semana**, para así facilitar la conciliación con las personas que trabajen y/o tengan que hacer largos desplazamientos.

Por otra parte, también debemos evitar que la fecha elegida sea en la medida de lo posible cercana a otros eventos de similar filosofía. Organizar un evento así requiere esfuerzo y dedicación, así que no deberíamos entorpecer el trabajo de otras que también intenten fomentar la cultura abierta y compartir conocimiento sobre tecnologías libres y cultura libre.

### Aspectos económicos

**es<span class="red">Libre</span> es un evento de asistencia gratuita que se organiza mediante la colaboración que voluntariamente ofrecen su tiempo para que salga adelante**. Conocemos la dificultad de llevar a cabo un evento así sin coste alguno para el público, pero por ahora no hemos tenido la necesidad de hacerlo de otra forma gracias a la disponibilidad de uso de espacios públicos e infraestructuras de las que disponían los anteriores grupos locales.

En cualquier caso, puede ser un punto importante conocer si el propio equipo organizador local tendría acceso a algún tipo de financiación o tiene intención de buscar patrocinadores que pudieran realizar aportaciones con el fin de mejorar cualquier aspecto del evento. Fuera el caso que fuera, **la única restricción que se pondría es que en caso de ser por parte de una empresa, esta no deberá tener una principal actividad que choque frontalmente con la filosofía del evento.**

Si existiera la posibilidad de cubrir este punto, comentar en qué se invertiría ese presupuesto: café y/o comida, packs de bienvenida (camisetas, acreditaciones, recuerdo local...), cartelería y medios físicos de difusión, grabación y retransmisión de las actividades...

### Facilidades disponibles

**Algunos aspectos para realizar el evento son imprescindibles, como disponer de un espacio físico asegurado**, pero hay varios aspectos que también se pueden tener en cuenta a la hora de mejorar el evento en general como puede ser disponibilidad de comida o cercanía para tomar café, posibilidad en conseguir descuentos para trasporte o la existencia de alojamientos a precios razonables cercanos a la sede del evento.

Teniendo en cuenta lo que se comentaba de la idoneidad de un modelo mixto para el congreso, también sería interesante conocer si dispondrían de medios para realizarlo: conexión a internet estable y de una velocidad aceptable, disponibilidad de servidores para montar soluciones del tipo [BigBlueButton](https://bigbluebutton.org/) o [PeerTube](https://joinpeertube.org/), personas dentro de la organización local con capacidad de gestionar estos elementos...

### Planificación y uso del espacio físico

Para facilitar la planificación también es importante conocer una serie de detalles sobre el espacio físico donde se desarrollará el congreso, cualquier detalle siempre es bienvenido, pero como mínimo sería necesario conocer los siguientes aspectos:

-   Espacios disponibles, así como la facilidad de acceso, en la propia sede para las actividades (número de salas disponibles, capacidad de la mismas, salón para actos de apertura y clausura)
-   Medios de transporte disponibles para llegar al lugar de la sede y facilidad para llegar a la propia sede: carretera, autobús, tren...
-   Si tendría espacio para alguna forma de conciliación de familias asistentes al congreso (talleres de robótica, actividades lúdicas al aire libre...)
-   Si se cuenta con el apoyo de alguna institución
-   Planes para actividades comunitarias fuera del horario del congreso (comida/cena de la comunidad, ruta turística/culturar, salida nocturna...)
-   Valoración post-evento

Para quien aproveche para viajar, seguramente también agradezca un pequeño resumen sobre la localidad que daría sede a este evento, tanto algunas cosas que la hagan característica como aspectos que puedan hacerla interesante e incluso puedan estar relacionadas con el evento (como espacios culturales en la provincia), en definitiva, cosas que podrían resultar especialmente llamativos para las personas que nunca hayan estado ahí. En el mismo caso, si la propia sede del congreso tiene alguna historia o valor cultural relevante 😎.

### Presentación de propuestas

Todas las propuestas se presentarán desde [este formulario](/2022/formulario-sede) y una vez recibidas quedarán accesibles desde [el mismo repositorio](https://gitlab.com/eslibre/coord/-/tree/master/propuestas/2022) que indicábamos antes.

El proceso será transparente en todo momento, y presentar las propuestas así es una _mera formalidad_ simplemente para simplificar el proceso, cualquier información se podrá ampliar o modificar en todo momento mientras que el plazo siga abierto, e igualmente si pensamos que falts alguna información importante nos pondríamos en contacto para informaros de ello.

El último día para presentar propuestas o modificarlas será el **14 de enero**, siendo publicada la lista definitiva nos más allá del **16 de enero**. La selección de propuestas presentadas se someterá a votación de la comunidad en los días consecutivos a disponer de la lista definitiva, para que la decisión que nos permita elegir el lugar más idóneo para el transcurso de este evento en las mejores condiciones posibles también sea algo colaborativo.

<p style="padding-top: 5px; text-align: center; font-size: 1.5rem;">El plazo está abierto hasta el <strong>14 de enero</strong> y puedes enviar tu propuesta desde <strong><a href="/2022/formulario-sede" target="_blank">este formulario</a>:<br> ¡propón tu sede para es<span class="red">Libre</span> 2022!</strong></p>

### Contacto

Para cualquier duda, cuestión o sugerencias podéis contactarnos por correo o cualquier de nuestras redes:
- Correo electrónico: [hola@eslib.re](mailto:hola@eslib.re)
- Matrix: [#esLibre:matrix.org](https://matrix.to/#/#esLibre:http://matrix.org)
- Telegram: [@esLibre](https://t.me/esLibre)
- Mastodon: [@eslibre@hostux.social](https://hostux.social/@eslibre)
- Twitter: [@esLibre_](https://twitter.com/esLibre_)
