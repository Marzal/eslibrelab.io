---
layout: 2022/default
section: info
permalink: /2022/info-xeral
title: Acerca de esLibre 2022
---

<h1 class="mt-3">
  <strong>es<span class="red">Libre</span></strong> Edición 2022
</h1>

<p align="center">
  <cite style="color: red;">Páxina con información que se actualizará regularmente.</cite>
</p>

**es<span class="red">Libre</span>** é un encontro de persoas interesadas nas tecnoloxías libres, centrada en compartir coñecemento e experiencia ao seu redor.

Despois de dúas edicións virtuais pola situación actual, por fin parece que poderemos volver a celebrar o evento presencialmente. Como resultado da convocatoria en sede aberta, a presentada por **[GALPon - Grupo de Amigos de Linux de Pontevedra](https://gitlab.com/eslibre/coord/-/blob/master/propuestas/2022/galpon-grupo-de-amigos-de-linux-de-pontevedra.md)**. **[GALPon](https://www.galpon.org/)** é unha asociación cultural sen ánimo de lucro que se dedica a promover e difundir o uso de **o software libre na sociedade galega**. O seu principal ámbito de actuación é a provincia de **Pontevedra, en Galicia**, España; onde protagonizaron eventos de gran relevancia para o software libre como **[Akademy-es 2019](https://www.kde-espana.org/akademy-es-2019)**. Ademais, tamén como coorganizadores desta edición, contarán co apoio de **[AGASOL (Asociación de Empresas Galegas de Software Libre Agasol)](https://www.agasol.gal/)**, unha entidade coa que veñen traballando historicamente tamén coa intención de mellorar a implantación da cultura libre nos diferentes sectores da comarca.

### Sobre o congreso

Este ano o congreso terá a opción de celebralo **presencialmente en Vigo** grazas a **GALPon** y **AGASOL**, como **comunidades anfitrionas**, encargáronse de conseguir os espazos necesarios en **[MARCO (Museo de Arte Contemporáneo de Vigo)](https://www.marcovigo.com/)** y **[MUTA (Espazo Mutante para Eventos no centro de Vigo)](https://www.muta.gal/)**.

<iframe width="100%" height="300px" frameborder="0" allowfullscreen src="//umap.openstreetmap.fr/es/map/vigo_akademy-es_308331?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=undefined&captionBar=false"></iframe><p><a href="//umap.openstreetmap.fr/es/map/vigo_akademy-es_308331">Ver pantalla completa</a></p>

Seguirase o mesmo formato que en anos anteriores: charlas, obradoiros, salas comunitarias... co apoio de asociacións como **[Interferencias](https://interferencias.tech/)** y **[LibreLabGRX](https://librelabgrx.cc/)** y outras organizacións como **[OfiLibre de la URJC](https://ofilibre.gitlab.io/)**, quen axudará á organización e sumará esforzos na realización desta edición nun formato híbrido presencial/virtual. Por último, contarán tamén como patrocinadores con a **[Xunta de Galicia](https://www.xunta.gal/)**, **[Librebit](https://www.librebit.com/es/)** e a **[Asociación MeLiSA (Asociación de usuarios de software libre da Terra de Melide)](https://www.melisa.gal/)**.

O **envío de propostas xa está oficialmente pechado**, polo que podes coñecer todas as actividades do congreso nas seguintes páxinas:

- **Horario completo**: <https://eslib.re/2022/horario/>
- **Propostas aceptadas**: <https://propuestas.eslib.re/2022/>

### Como chegar

Se **vés de fóra de Vigo en avión**, recomendámosche que non collas un taxi directamente para chegar dende o aeroporto sen parar antes a mirar o transporte público:
- Para comezar, o **autobús urbano** é moito máis barato: o billete custa 1.35€ fronte ao "mínimo" duns 22€ do taxi. Recomendando dispor do importe exacto ou ter en conta que no se pode pagar con billetes de importe superior a 10 €.
- [Autobuses da **liña A**](https://www.vitrasa.es/renovaciones-85_A) pasan polo aeroporto cada **30-35 minutos**, simplemente colle un autobús desa liña, e se baixas na parada **Rúa de Urzaiz / Rúa do Príncipe** (unha viaxe duns 30 minutos), xa estarás na porta do MARCO. Deixámosvos [aquí](/2022/assets/pdf/Vitrasa.pdf) tamén un PDF coas paradas da liña A que che poden interesar destacadas.
- Tamén podes planificar as túas viaxes tanto para a viaxe de ida como a de volta usando a páxina de Moovit
  * Á chegada: [Aeroporto de Vigo (VGO) → Rúa Do Príncipe, Vigo](https://moovitapp.com/vigo-3841/poi/R%C3%BAa%20Do%20Pr%C3%ADncipe/Aeropuerto%20de%20Vigo%20(VGO)/es?tll=42.236517_-8.721962&fll=42.224978_-8.632835&utm_medium=link&utm_source=shared&customerId=6863)
  * Para o regreso: [Rúa Do Príncipe, Vigo → Aeroporto de Vigo (VGO)](https://moovitapp.com/vigo-3841/poi/Aeropuerto%20de%20Vigo%20(VGO)/R%C3%BAa%20Do%20Pr%C3%ADncipe/es?tll=42.224555_-8.631747&fll=42.236517_-8.721962&utm_medium=link&utm_source=shared&customerId=6863)
- En todo caso, tamén se poden consultar todas as liñas na páxina web da empresa responsable do transporte público urbano da cidade de Vigo: [VITRASA](https://www.vitrasa.es/renovaciones) ou use [esta páxina](https://www.vitrasa.es/php/index.php?pag=calculo-rutas) para calcular rutas entre diferentes paradas.

Se **tamén vés de fóra de Vigo pero en tren**, unha vez que saias da estación estarás practicamente no centro da cidade, e en só 10 minutos camiñando estarás no MARCO.

### Turismo

Se queres aproveitar para facer tamén algo de turismo, podes atopar información interesante na páxina do [Concello de Vigo](https://www.turismodevigo.org/gl). En concreto son moi suxestivos os "Paseos pola Arquitectura" dos que o propio Concello destaca (tamén todo o próximo ao MARCO):

- [Vigo Antigo: introdúcete nas ruelas rodeadas de edificios históricos](https://www.turismodevigo.org/gl/vigo-antigo)
- [Vigo Marítimo: un percorrido entre o mar e a pedra](https://www.turismodevigo.org/gl/vigo-maritimo)
- [Vigo Señorial: ruta pola cidade burguesa](https://www.turismodevigo.org/gl/vigo-senorial)
- [Vigo de onte a hoxe: a cidade a través de 7 séculos](https://www.turismodevigo.org/gl/vigo-de-onte-hoxe)

### Exposicións

No propio MARCO:
- Na sala de exposicións da primeira planta, exposición de pinturas debuxos e esculturas de Alfredo Alcain
- Na sala de exposicións da planta baixa, exposición de «O antropomórfico 1996-2022» de Francisco Leiro.

+info en:
- <https://www.marcovigo.com/es/actuais>
- <https://www.nosdiario.gal/articulo/cultura/corpos-atormentados-francisco-leiro/20220525091846144186.html>

### Rexistro asistentes

<p style="padding-top: 5px; text-align: center; font-size: 1.25rem;"><strong>A asistencia ao evento é <span class="red">libre e de balde</span>, máis por cuestións de capacidade necesitamos que te rexistres <a href="https://eventos.librelabgrx.cc/events/2e86b318-eed2-4e95-84da-42f8ea0337f9" target="_blank">aquí</a> se tes no maxín asistir.</strong></p>

<p style="text-align: center;">Se tes algunha dúbida, non dubides en escribirnos a <a href="mailto:hola@eslib.re">hola@eslib.re</a>.</p>

<h3 style="padding-top: 10px; text-align: center;">ATA PRONTO!!!<br>👏👏👏</h3>
