---
layout: 2022/default
section: acerca
permalink: /2022/bolsa-viaje
title: Solicitud de fondos para participar en esLibre
---

<h1 class="mt-3">
  Solicitud de fondos para participar en <strong>es<span class="red">Libre</span></strong>
</h1>

Este año además de volver a tener **edición presencial**, también podemos anunciar que tendremos la oportunidad de ayudar a financiar los gastos de viaje a algunas personas que quieran participar en **es<span class="red">Libre</span>**.

Hemos decidido que el objetivo fundamental de de las ayudas sea apoyar la participación de colectivos  en nuestro congreso, especialmente colectivos de **género** (mujeres, personas trans y minorías de género infrarrepresentadas en **es<span class="red">Libre</span>**), además de apoyar también a personas en situación de discapacidad, personas neurodivergentes y/o neurodiversas y personas con enfermedades. El objetivo fundamental de esta estrategia es crear un espacio seguro en el que poder debatir sobre software libre poniendo de manifiesto realidades diversas y su relación con la tecnología.

También queremos ofrecer una ayuda general a las personas que, por su situación económica, tengan problemas para cubrir el coste del viaje y estancia durante el evento. Por ejemplo, consideraremos de forma especial la situación de estudiantes, personas jubiladas, personas en situación de desempleo o con empleo precario. Los criterios y la proporción de las ayudas serán personalizadas para cada caso, así que no dudes en consultar.

<p style="text-align: center;"><strong>Para optar a las ayudas a las personas en todas estas situaciones, basta con indicarlo en el <a href="/2022/enviar-propuestas" target="_blank">formulario de envío de propuestas</a>.</strong></p>

En **es<span class="red">Libre</span>** queremos impulsar charlas, talleres y actividades que no tengan necesariamente que ser técnicas, si no que puedan ser conversaciones de varias áreas (humanidades, sociales, artísticas, etc) sobre la tecnología libre. Este proceso intenta conseguir una **es<span class="red">Libre</span>** más diversa mediante un apoyo directo a cualquiera que quiera hablar de software libre, independientemente de su situación y contexto personal. Se priorizará la concesión de ayudas a las personas solicitantes en función del impacto que estimemos que tendría su propuesta en el programa del congreso.

Por otro lado, si eres una **persona que podrían optar a estas ayudas** pero **no te sientes cómoda participando activamente** enviando en ningún tipo de propuesta, usa **[este formulario](/2022/form-bolsa)** para ponerte en contacto y veremos cómo podemos ayudarte a asistir al congreso.

<p style="text-align: center;"><strong>Para cualquier duda que tengas, no dudes en escribirnos a <a href="mailto:hola@eslib.re">hola@eslib.re</a>.</strong></p>

<div class="media">
  <img style="display: block !important; margin-left: auto !important; margin-right: auto; !important; max-width: 60% !important; height: auto !important;" src="/2022/assets/imgs/banner_anuncio.png" alt="Logo esLibre" class="jumbotron mt-3 embed-responsive">
</div>
