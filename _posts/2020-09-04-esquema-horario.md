---
layout: 2020/post
section: post
category: 2020
title: Esquema horario del congreso / Conference schedule
---

**[See English version below]** Cada vez estamos más cerca de que llegue el día de celebrar <strong>es<span class="red">Libre</span> 2020</strong>, así que finalmente hemos decidido que el esquema horario que seguirán ambas jornadas del congreso será tal que así (todas las horas son según hora central europea de verano [CEST]):

  * 10:00-10:50
    * VIERNES. Bienvenida al congreso y sesión plenaria de presentación
    * SÁBADO. Sesión plenaria invitada
  * 11:00-12:15 Sesiones paralelas / Actividades de las salas
  * 12:15-12:30 Descanso
  * 12:30-13:45 Sesiones paralelas / Actividades de las salas
  * 13:45-15:00 Comida
  * 15:00-15:45 Sesión plenaria invitada
  * 16:00-17:15 Sesiones paralelas / Actividades de las salas
  * 17:15-17:30 Descanso
  * 17:30-18:45 Sesiones paralelas / Actividades de las salas
  * 19:00-19:45
    * VIERNES. Evento social online
    * SÁBADO. Desconferencia: futuro de esLibre, comunidad del software libre en España, propuestas y clausura

La idea principal es que en todo momento podamos suplir la calidez del trato en persona con un planteamiento más dinámico de las actividades del congreso. Dentro de poco también os confirmaremos tanto los eventos sociales que organicemos para todo el público asistente virtual como las facilidades que proporcionaremos para las personas que se quieran reunir en pequeños grupos privados.

El programa completo de actividades ya confirmadas podéis encontrarlo en la [página de propuestas](https://propuestas.eslib.re/2020/), y en los próximos días publicaremos en la [página del programa]({{ site.url }}/2020/programa) el horario final de todas las actividades, tanto las de las sesiones propias de <strong>es<span class="red">Libre</span></strong> como las de las salas de las comunidades organizadas también dentro de <strong>es<span class="red">Libre</span></strong>.

**¡Esto ya está en marcha!**

👏👏👏 🐧💻

<hr>

We are getting closer and closer to the day of celebrating <strong>es<span class="red">Libre</span> 2020</strong>, so finally we decided that the schedule scheme that will the congress follow both days will be like this (all hours are according to Central European Summer Time [CEST]):

  * 10:00-10:50
    * FRIDAY. Welcome to the congress and plenary presentation session
    * SATURDAY. Invited plenary session
  * 11:00-12:15 Parallel sessions / Activities in the rooms
  * 12:15-12:30 Break
  * 12:30-13:45 Parallel sessions / Activities in the rooms
  * 13:45-15:00 Lunch
  * 15:00-15:45 Invited plenary session
  * 16:00-17:15 Parallel sessions / Activities in the rooms
  * 17:15-17:30 Break
  * 17:30-18:45 Parallel sessions / Activities in the rooms
  * 19:00-19:45
    * FRIDAY. Online social event
    * SATURDAY. Disconference: future of esLibre, free software community in Spain, proposals and closure

The main idea is that at all times we can replace the warmth of in person contact with a more dynamic approach of the activities of the congress. Shortly we will also confirm both the social events that we organize for the entire virtual assistant public and the facilities that we will provide for people who want to meet in small private groups.

You can find the full program of confirmed activities on the [page of proposals](https://propuestas.eslib.re/2020/), and in the next few days we will publish on the [program page]({{ site.url }}/2020/programa) the final schedule of all activities, both those of the <strong>es<span class="red">Libre</span></strong> sessions like those of the community rooms organized within <strong>es<span class="red">Libre</span></strong>.

**This is already underway!**

👏👏👏 🐧💻
