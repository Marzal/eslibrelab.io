---
layout: 2020/post
section: post
category: 2020
title: Presentación de esLibre 2020 virtual / Presenting virtual esLibre 2020
---

**[See English version below]** El próximo miércoles 5 de agosto, a las 18:00 CEST / 16:00 UTC tendrá lugar la presentación de la edición 2020 de esLibre, que tendrá lugar de forma virtual. La presentación tendrá lugar en una [sala de una instancia de BigBlueButton](https://bbb.eslibre.urjc.es/b/jes-b17-myc) que estamos desplegando para que nos sirva como punto central del congreso. Durante la presentación explicaremos cómo estará organizado el congreso, qué herramientas utilizaremos, y cómo podrás conectarte para seguirlo todo.

Además, utilizaremos esta presentación como una primera prueba de carga y rendimiento, así que si tienes un ratito únete (sólo te hará falta un navegador web), y ayúdanos.

Recuerda:

* Cuándo: Miércoles 5 de agosto, 18:00 CEST / 16:00 UTC
* Cómo: https://bbb.eslibre.urjc.es/b/jes-b17-myc

<hr>

Next Wednesday, August 5th, at 18:00 CEST / 16:00 UTC, we will present the esLibre 2020 edition, which will be held virtually. The presentation will happen in a [BigBlueButton room](https://bbb.eslibre.urjc.es/b/jes-b17-myc), of the instance that we're deploying as the central point for the conference. During the presentation we will explain how the conference will be organized, which tools we will use, and how to connect to follow all the action.

In addition, we will use this presentation as a first load and performance test, so if you can, please join us (you will need just a web browser), and help us.

Remember:

* When: Friday, August 5th, 18:00 CEST / 16:00 UTC
* How: https://bbb.eslibre.urjc.es/b/jes-b17-myc
