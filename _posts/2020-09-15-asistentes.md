---
layout: 2020/post
section: post
category: 2020
title: esLibre 2020 (información para asistentes)
---

El viernes 18 de septiembre comienza [esLibre 2020](https://eslib.re), este año en modalidad completamente virtual, y con un [programa](/2020/programa/) bien completito. Para asistir a él, te bastará con un navegador y ganas de conocer mejor el mundo del software libre, las tecnologías libres, y el conocimiento libre. No hay que registrarse, y es completamente gratis. esLibre es organizado por voluntarios, con el apoyo este año de la [OfiLibre](https://ofilibre.gitlab.io/) de la [URJC](https://urjc.es) y [otras organizaciones](/2020/acerca/).

## Si necesitas ayuda...

Antes de continuar: si necesitas ayuda (técnica, porque tienes problemas con las herramientas, o de otro tipo), puedes preguntar en el [canal de ayuda de RocketChat](https://rocket.eslibre.urjc.es/channel/ayuda) (tendrás que crearte una cuenta primero). Eso sí, antes de pedir ayuda, mira a ver si localizas lo que te haga falta a partir de los enlaces en esta nota, por favor.

## Estructura, pistas, salas...

El congreso esLibre está estructurado en:

* Actividades plenarias: apertura, cierre, y charlas plenarias.
* Charlas: la mayoría son de 20 minutos, hay alguna de 10 minutos. En cualquier caso, tendrán 5 minutos adicionales para preguntas y comentarios.
* Talleres: normalmente en bloques de 75 minutos (uno o varios bloques)
* Salas (devrooms): cada una con su propio programa temático, con su propio grupo organizador, discurriendo normalmente en varios bloques de 75 minutos.

Las actividades plenarias irán en una pista (track) propia: la Pista Plenaria. No habrá otras actividades simultáneas a las plenarias. Las charlas y talleres irán en tres pistas que discurrirán simultáneamente: Pista I, Pista II y Pista III. Las salas irán cada una en su propia "pista" (que para ser originales, llamaremos "sala": Sala Conocimiento Abierto, Sala Haskell, Sala Derechos Digitales, Sala GNOME, Sala AndaluGeeks), y que serán también simultáneas a las pistas. Por lo tanto, cuando no estemos en un periodo plenario, tendrás que elegir a qué pista o sala quieres ir.

Temporalmente, el programa está estructurado en sesiones, de unos 60 minutos (plenarias) o de 75 minutos (pistas "normales" y salas). Entre sesiones, puede haber descansos. Hemos procurado que no haya demasiados contenidos seguidos sin descanso, para que el congreso no se haga demasiado pesado. Pueden usarse las facilidades que se ofrecen para tener reuniones con otros asistentes durante estos periodos (ver más abajo).

Además, al principio de cada mañana hemos previsto un periodo para conexión y pruebas donde todos los asistentes deberían ir conectándose y probando las herramientas, para resolver cualquier problema que pueda surgir, y poder comenzar las sesiones puntualmente.

La información fundamental para acceder a pistas y salas está en el [programa](/2020/programa/). Verás en él varias columnas: cada una corresponde a una pista o a una sala que ocurre en paralelo con las demás. Cada pista o sala tiene asignados un canal de RocketChat (para mensajería instantánea) y una habitación de BigBlueButton (para videoconferencia). Ojo, aunque el canal de RocketChat se usará siempre para las pistas del congreso (charlas y talleres), algunas salas pueden decidir no usarlo: en ese caso, se comentará a los asistentes a esa sala.

En el caso de los talleres, por favor consultad con cuidado en la descripción del taller qué hace falta para poder seguir, porque algún taller podría necesitar que te instales alguna herramienta específica.

Tened en cuenta que trataremos de seguir el programa de la forma más puntual posible. Eso quiere decir que las charlas, y los periodos de preguntas y comentarios, tendrán que terminar también puntualmente. Si queréis seguir la discusión, por  favor moverla a otro canal (siempre habrá opciones), para seguir hablando allí sin molestar a la siguiente ponencia.

Tanto en RocketChat como en BigBlueButton, recomendamos abrirse cuenta con nombre real, para que te identifiquen fácilmente otros asistentes, y en general, con el mismo nombre en BBB y en RocketChat. Como las charlas en BBB se grabarán, tendrás que dar consentimiento para entrar en las salas correspondientes (porque BBB podría grabar alguna interacción por tu lado).


## Uso de BigBlueButton

[https://bbb.eslibre.urjc.es](https://bbb.eslibre.urjc.es)

BigBlueButton (BBB) se usará fundamentalmente para videoconferencia. El ponente de un taller o una charla presentará usando la herramienta, normalmente usando su cámara, su micrófono y sus transparencias. Algunos ponentes puede que pongan vídeos con su charla, en lugar de hacer presentaciones en vivo, por motivos extraordinarios. BBB será usado también por el moderador de cada sesión para presentar a los ponentes, coordinar los periodos de comentarios y preguntas, etc. En general, tratará de estar al tanto de las preguntas que haya en RocketChat (especialmente por gente que no los pueda hacer de palabra), para transmitirlos al ponente durante el periodo de preguntas.

Hay un canal específico para cada pista, llamados "Pista I", "Pista II", "Pista III" y "Pista Plenaria", y otro para cada sala. Las urls de estos canales están disponibles en el programa: entra en BBB siguiendo esos enlaces. Se grabará lo que ocurra en BBB durante la charla, las grabaciones estarán disponibles un tiempo después. Ojo, que no estamos 100% seguros de que las grabaciones funcionen bien: quien quiera asegurarse, aconsejamos que alguien haga la grabación localmente, con [OBS](https://obsproject.com/) por ejemplo, por si nuestra grabación falla.


## Uso de RocketChat

[https://rocket.eslibre.urjc.es/](https://rocket.eslibre.urjc.es/)

Como el chat de BBB es relativamente simple, y puede ser difícil seguirlo si mucha gente está escribiendo a la vez, vamos a usar en general RocketChat (Rocket) para la mensajería instantánea asociada a las pistas y a las salas que quieran usarlo. Entrad directamente por los enlaces a los canales de las pistas y las salas, que podéis ver en el programa. La primera vez que trates de entrar, tendrás que crearte una cuenta.

Hemos abierto varios canales en Rocket, uno por pista y uno por sala. Además, tenemos los canales `#general` y `#ayuda`. El primero es para cualquier asunto general, relativo a esLibre, que quieras comentar, y el segundo, para cualquier tipo de ayuda relacionada con el congreso que necesites. La organización trataremos de seguir lo que ocurre en ambos canales, y en particular en el de ayuda. Como además en general Rocket suele plantear menos problemas de funcionamiento, es el lugar donde podrás preguntar si no puedes conectarte a BBB. Por último, `#ayuda` funcionará también como suelen funcionar las mesas de acogida en los congresos físicos: ahí te podremos dar cualquier información que precises sobre el congreso.

En Rocket podrás comentar (ya en el canal específico de una pista o de una sala) lo que está ocurriendo en una charla, tus opiniones sobre una ponencia, información complementaria, o hacer preguntas si no puedes hacerlas con el micro. En general, los moderadores de las sesiones tratarán de seguir lo que ocurra en estos canales, para poder hacer las preguntas al ponente en tu nombre.

## Otros servicios

Se han desplegado también instancias de Mumble y Jitsi, que en general no harán falta salvo que tengamos que pasar a un plan B, por problemas con las herramientas principales.

## Evento social

Habrá un evento social el viernes a las 19:00 en [Mozilla Hubs](https://hubs.mozilla.com/), si todo va bien en unas salas de realidad virtual personalizadas para esLibre. Anímate y ven. Todo lo que te hará falta es un navegador de escritorio (si tienes un dispositivo de realidad virtual que soporte WebVR en el navegador, también podrás usarlo). Daremos los enlaces a las salas más adelante. Además de poder curiosear un poco qué se puede hacer en Mozilla Hubs, podrás charlar un poco con otros asistentes a esLibre, y pasar un rato agradable. Eso sí, si quieres beber algo mientras el evento, tendrás que traértelo ;-).

## Redes sociales

Tenemos presencia en Twitter [@esLibre_](https://twitter.com/esLibre_) y Mastodon [@eslibre@hostux.social](https://hostux.social/@esLibre): Puedes seguirnos, y/o incluir el hashtag `#esLibre2020` en tus tuits/toots.

También tenemos un canal espejado [en Telegram](https://t.me/esLibre) y [en Matrix](https://matrix.to/#/!pBzNmqAIdyWatNSqNl:matrix.org) (`#esLibre:matrix.org`), únete si te apetece.

## Normas de convivencia

Recordad que hay un [Código de Conducta](https://eslib.re/2020/conducta/) de esLibre. Por favor, respétalo.
