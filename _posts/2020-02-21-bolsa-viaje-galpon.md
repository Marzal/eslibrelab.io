---
layout: 2020/post
section: post
category: 2020
title: Bolsa de viaje para esLibre 2020 de GALPon
---

La buena gente de GALPon quiere facilitar que las personas interesadas en asistir a la edición de esLibre de este año no tengan impedimiento en hacerlo por motivos económicos; por eso mismo, han creado una bolsa para ayudar a financiar el viaje al congreso. Por lo pronto destinarán un máximo de 600,00€ para hacerse cargo de un mínimo de 3 viajes al evento.

Cualquier persona puede solicitar esta ayuda, pero estas serán atendidas en orden de prioridad según los siguientes elementos favorables no excluyentes:

<ul>
<li>Presente una charla, organice (o colabore en la organización) una sala, presente un taller o alguna otra propuesta de aportación/colaboración</li>
<li>Sea estudiante de FP o universitario</li>
<li>Esté realizando prácticas de maestrado o FP o sea becario en una empresa privada (no universidades o asimilables)</li>
<li>Sea socia o colaboradora de GALPon</li>
<li>En el caso de igualdad de méritos para recibir la ayuda tendrán preferencia las mujeres</li>
</ul>

Para hacer la solicitud, quien lo desee deberá enviar un correo a <strong><a href="mailto:xornadas@galpon.org">jornadas galpon</a></strong> aportando toda la información que consideren necesaria para acreditar su mérito.

Más información en la web de GALPon: <https://www.galpon.org/content/bolsa-de-viaxe-para-asistir-ao-congreso-eslibre-2020>.
