---
layout: 2021/post
section: post
category: 2021
title: ¡Envío de propuestas abierto para esLibre 2021!
---


Nos complace anunciar que la edición de **esLibre 2021** se celebrará de manera online los días **25 y 26 de Junio de 2021** totalmente online.

Puedes lanzar propuestas diferentes:

-   **Charlas**. Presentación "tradicional", en formato normal (25 min. aproximadamente) o en formato relámpago (10 min. aproximadamente). La organización podrá proponer cambios de formato a alguna de las charlas, por motivos organizativos o de contenidos. También podrá, de forma excepcional, proponer a alguna charla un formato más largo.

-   **Talleres**. Presentación práctica, "manos en la masa". Puede ser en muchos formatos, desde demostraciones donde las personas que asistan puedan seguir una especie de "paseo guiado" por la temática del taller, hasta sesiones de iniciación a una tecnología donde se pueda experimentar con ella; en general, cualquier formato práctico que puedas considerar que sea interesante para dar a conocer algo.

-   **Salas (que quizas te suenen como devroom)**. También se puede proponer el programa de una sala, que se realizaría en paralelo con el resto de actividades del congreso. Normalmente una sala estará organizada por y/o para una comunidad, y habitualmente será especializada en un tema (aunque también puede ser generalista). Sólo como ejemplo, en ediciones anteriores tuvimos salas que trataban temática más generales como el software libre en la Universidad, privacidad y derechos digitales o la divulgación de la cultura libre; y otras más específicas como Perl/Raku, GNOME, la programación funcional o tecnologías para el fomento de la lengua andaluza.

Os presentamos también dos nuevos tipos de propuestas:

- **Sección de artículos**. Puedes considerar esto como una sección de "pósters virtuales", donde dedicaremos un apartado de la web a exponer todas aquellas publicaciones que hayas realizado y que te gustaría que las personas pudieran conocer, dando si quieres un medio de contacto por el que te pudieran plantear cuestiones o simples comentarios. Además, se aprovecharía para anunciarlos en los descansos del congreso.

-   **Tablón de proyectos**. Si hay un proyecto de software libre de cualquier ámbito sobre el que te gustaría que se hiciera eco, pero prefieres que sea de una forma más informal o sin tener que asumir el foco de atención como en los tipos de actividades anteriores, también puedes proponerlo y pondremos los medios para reunir gente que pueda estar interesada en el mismo proyecto.

Esperamos vuestras propuestas así que... ¿A qué estás esperando para [enviar tu propuesta](https://eslib.re/2021/propuestas/)? 😏😏
