---
layout: 2020/post
section: post
category: 2020
title: Aplazamiento indefinido / Indefinite postponement
---

**[See English version below]** Debido a la situación actual producida por el COVID-19, es un hecho que la edición de esLibre de este año no se podrá realizar ni en las fechas originalmente anunciadas, ni con el formato planteado; por lo que hemos estado pensando todo este tiempo cual sería la mejor alternativa.

Además, durante este periodo también hemos visto que se han llevado a cabo un gran número de eventos, charlas y talleres online; así que por suerte tampoco estamos faltos de todo tipo de actividades.

esLibre tiene un mayor sentido como un evento presencial donde tener la oportunidad de reunirse en comunidad, para que personas con los mismos intereses en tecnologías libres puedan compartir conocimiento y experiencias de la forma más amigable posible.

Es por eso que la edición de este año de esLibre queda aplazada de forma indefinida mientras que no se pueda asegurar que no existe ningún riesgo para la salud pública. No descartamos realizar actividades online, pero en cualquier caso comunicaremos cuando haya cualquier novedad.

Para cualquier pregunta podéis escribirnos a [propuestas@eslib.re](mailto:propuestas@eslib.re). Lo sentimos mucho y esperamos poder vernos pronto.

Gracias.

<hr>

Due to the current situation produced by COVID-19, it is a fact that this year's esLibre edition will not be able to be carried out either on the dates originally announced, or in the format proposed; so we have been thinking all this time what would be the best alternative.

Furthermore, during this period we have also seen that a large number of online events, talks and workshops have been held; so luckily we are not lacking in all kinds of activities.

esLibre makes more sense as an in-person event where to have the opportunity to meet in community, so that people with the same interests in free technologies can share knowledge and experiences in the most friendly way possible.

That is why this year's edition of esLibre is postponed indefinitely while we cannot be ensured that there is no risk to public health. We do not rule out online activities, but in any case we will communicate when there is any news.

For any question, you can write to [propuestas@eslib.re](mailto:propuestas@eslib.re). We are very sorry and hope to see you soon.

Thanks.
