---
layout: 2021/default
section: propuestas
title: Envío de propuestas para esLibre 2021
description: Nos interesa conocer qué cosas haces. ¡Mándanos propuestas!
---

<h1 class="mt-3 mb-3">Envío de propuestas para <strong>es<span class="red">Libre</span></strong> 2021</h1>

<p align="center">
  <cite style="color: black;">You can find an English version of this text <a href="/2021/proposals/">here</a>.</cite>
</p>

<p>
  <cite style="color: grey;">¿Conoces a alguien que podría querer hablar en el congreso? Envíale esta página a quien quieras, te lo agradeceremos un montón si nos ayudas a llegar a más gente.</cite>
</p>

El [congreso **es<span class="red">Libre</span>** ](https://eslib.re)es un encuentro de _personas interesadas en las [tecnologías libres](https://www.gnu.org/philosophy/free-sw.es.html) y la [cultura libre](https://es.wikipedia.org/wiki/Cultura_libre), enfocado a compartir conocimiento y experiencias alrededor de las mismas_. La próxima edición volverá a ser retransmitida de forma online los días viernes 25 y sábado 26 de junio, siendo en esta ocasión la comunidad anfitriona **[LibreLabUCM](https://librelabucm.org/)**, contando también con el apoyo de las comunidades que ya organizaron las ediciones pasadas: **[Interferencias](https://interferencias.tech/)**, **[LibreLabGRX](https://librelabgrx.cc)** y la **[OfiLibre de la URJC](https://ofilibre.gitlab.io/)**.

En **es<span class="red">Libre</span>** intentamos mantener un evento anual donde todo el mundo está invitado a participar, lo único que necesita tener algo que decir sobre el mundo de las tecnologías y cultura libre. Nos interesan personas de todas las edades, experiencias y perfiles ya sean técnicas o usuarias. Estamos deseando escuchar y aprender de todas las personas y nuestro objetivo principal es llegar a comunidades de todo tipo.

<h3 class="mt-3 mb-3" id="tematica">Temática</h3>

El **software libre** es la temática central de **es<span class="red">Libre</span>**, pero lo que realmente queremos es conseguir un evento donde encontremos tanto actividades técnicas como otras de un nivel más divulgativo, o incluso que den pie a debates para compartir diferentes puntos de vista sobre aspectos del mundo del conocimiento libre. Esto puede comprender comentarios sobre licencias de publicación, aplicaciones y desarrollo de diversas tecnologias, intereses e inquietudes que quieran presentar diferentes comunidades, o la exploración y divulgación del arte libre.

En líneas generales, _temas relacionados directamente con obras (software, hardware, cultura, etc.) que se distribuyan con licencias libres_, entendiendo las mismas según la [definición de la FSF de software libre](https://www.gnu.org/philosophy/free-sw.html) o la [definición de OSI de open source](https://opensource.org/osd); o en un ámbito más divulgativo, que se entienda que cumplan la [definición de obra cultural libre](https://freedomdefined.org/Definition/Es): todo esto es lo que se considera que encajan perfectamente en **es<span class="red">Libre</span>**. ¿Tienes algo en mente? ¡Anímate!

Aunque mucho software libre (y en general obras libres) se distribuye comercialmente sin que esto presente problema alguno, queremos mantener **es<span class="red">Libre</span>** como un foro neutro con respecto a los intereses comerciales. <u>En general, no se aceptarán charlas, talleres ni ningún otro tipo de actividad que promocionen específicamente una oferta comercial</u>.

<h3 class="mt-3 mb-3" id="tipos">Tipos de propuestas</h3>

Hay varios tipos de contribuciones que puedes proponer (pero no solo):

-   **Charlas**. Presentación "tradicional", en formato normal (25 min. aproximadamente) o en formato relámpago (10 min. aproximadamente). La organización podrá proponer cambios de formato a alguna de las charlas, por motivos organizativos o de contenidos. También podrá, de forma excepcional, proponer a alguna charla un formato más largo.

-   **Talleres**. Presentación práctica, "manos en la masa". Puede ser en muchos formatos, desde demostraciones donde las personas que asistan puedan seguir una especie de "paseo guiado" por la temática del taller, hasta sesiones de iniciación a una tecnología donde se pueda experimentar con ella; en general, cualquier formato práctico que puedas considerar que sea interesante para dar a conocer algo.

-   **Salas (que quizas te suenen como devroom)**. También se puede proponer el programa de una sala, que se realizaría en paralelo con el resto de actividades del congreso. Normalmente una sala estará organizada por y/o para una comunidad, y habitualmente será especializada en un tema (aunque también puede ser generalista). Sólo como ejemplo, en ediciones anteriores tuvimos salas que trataban temática más generales como el software libre en la Universidad, privacidad y derechos digitales o la divulgación de la cultura libre; y otras más específicas como Perl/Raku, GNOME, la programación funcional o tecnologías para el fomento de la lengua andaluza.

-   **Sección de artículos**. Puedes considerar esto como una sección de "pósters virtuales", donde dedicaremos un apartado de la web a exponer todas aquellas publicaciones que hayas realizado y que te gustaría que las personas pudieran conocer, dando si quieres un medio de contacto por el que te pudieran plantear cuestiones o simples comentarios. Además, se aprovecharía para anunciarlos en los descansos del congreso.

-   **Tablón de proyectos**. Si hay un proyecto de software libre de cualquier ámbito sobre el que te gustaría que se hiciera eco, pero prefieres que sea de una forma más informal o sin tener que asumir el foco de atención como en los tipos de actividades anteriores, también puedes proponerlo y pondremos los medios para reunir gente que pueda estar interesada en el mismo proyecto.

-   **Otras actividades**. ¡Usa tu imaginación! Propón otros formatos, tenemos mucho interés en explorar otras formas de compartir conocimiento (y más ahora que no nos separan barreras físicas 🐧).

<h3 class="mt-3 mb-3" id="como">Me interesa impartir una charla pero... ¿cómo?</h3>

¿Nunca has impartido una charla pero quieres estrenarte? En este evento nos encantaría acompañarte en esa aventura. Somos conscientes de que dar una charla puede ser un reto importante y por eso queremos darte una serie de pautas que te ayuden a exponer aquí (o donde sea). Te dejamos una lista de recursos para exponer sin usar un "powerpoint" (🙃): [Markdown](https://markdown.es/sintaxis-markdown/), [LaTeX](https://es.wikibooks.org/wiki/Manual_de_LaTeX/Texto_completo), [Marp](https://www.genbeta.com/herramientas/marp-herramienta-que-nos-permite-crear-presentaciones-modo-texto-usando-markdown), [Pandoc](https://ondiz.github.io/cursoLatex/Contenido/15.Pandoc.html)...

-   La gente de TED ha creado una [lista de vídeos](https://www.ted.com/playlists/574/how_to_make_a_great_presentation) que enseñan a hacer presentaciones.

-   Al ser un congreso online, necesitarás como mínimo un micró, una webcam y el ordenador (o un móvil en última instancia). Puedes prácticar a hablar frente al ordenador llamando a tu familia o amistades por [Jitsi](https://meet.jit.si/) y será casi igual que cuando lo hagas de verdad.

-   No escribas mucho texto en la presentación, haz una lista de puntos claves y desarróllalos hablando. Como es online, puedes tener una chuleta y nadie se dara cuenta (😉), pero procura no leer todo el tiempo o no quedará igual de fluido. ¡Dejate llevar! Piensa que estas en una conversación con alguien cercano.

-   Para que las y los asistentes que te escuhen puedan seguirte mejor, crea un índice exponiendo las ideas de las que quieres hablar y sigue ese orden durante la charla.

-   Acaba con un resumen que deje en las personas que te están escuchando con una idea que llevarse a sus vidas, así podrás crear un impacto mucho más interesante.

De nuevo, todo esto son sugerencias. Crea tu presentación como quieras y si necesitas más guía y ayuda no dudes en preguntar por ella a [la organización](mailto:propuestas@eslib.re). Para inspirarte, te dejamos una lista de charlas online que puedes ver ahora mismo:

-   [Miriam González: Sistema de diseño para dummies (TotoConf)](https://www.youtube.com/watch?v=ht6-jX8YF38)
-   [Ana Valdivia: Cómo aplicar el feminismo a los datos (TotoConf)](https://www.youtube.com/watch?v=VGfoq5WO0Kc)
-   [Erika Heidi: The art of programming (Codeland)](https://www.youtube.com/watch?v=1snO9k2gOu4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=6)
-   [Joe Karlsson: An introduction to IoT](https://www.youtube.com/watch?v=zHvrtt5raA4&list=PLyLTyCCJDTTfpr3LS731t47LnXTUdr4Bi&index=13)

Ahí fuera hay un montón de proyectos interesantes libres en los que participar, o usar. Puedes empezar [buscando un sistema operativo libre](https://distrowatch.com/) que vaya acorde con tu personalidad, leer algunos proyectos de desarrollo web en un [blog libre](https://dev.to/) o de metasoftware-libre en [este otro](http://www.elbinario.net/). También puedes explorar las diferentes redes sociales libres del [Fediverso](https://fediverse.party/) como [Mastodon](https://joinmastodon.org/). Date un paseo por estos proyectos y si te gustan, ven al evento para aprender sobre muchos más.

<h3 class="mt-3 mb-3" id="diversidad">Nuestro compromiso por la diversidad</h3>

**La organización [se compromete](/conducta) a crear un ambiente seguro, no tóxico y diverso**, de modo que nos encantaría dar voz a ponentes de las varias comunidades minoritarias que hagan uso o desarrollen software, hardware o cultura libre. Somos conscientes de que hay muchas personas que usan y crean tecnologías libre y apoyan activamente la divulgación de la cultura libre, y muchas veces no son escuchadas, queremos cambiar eso: ofrecemos nuestro micrófono y espacio para que se hable desde estas comunidades.

Queremos aprender y crear puentes, para que la tecnología sea (al fin) un herramientas de apoyo y colaboración. Si crees que en tu comunidad tenéis algo que contar desde el desarrollo o la experiencia usando software libre, háznoslo saber. Buscamos un futuro colaborativo, multidisciplinar y libre, ¡ayúdanos a conseguirlo!

<h3 class="mt-3 mb-3" id="fechas">Fechas</h3>

-   Fecha límite de propuestas: **4 de junio**
-   Fecha límite del programa de las salas: **11 de junio** (ese día tendrán que tener su programa finalizado)
-   Publicación del programa definitivo: **11 de junio**
-   Celebración del congreso **es<span class="red">Libre</span>**: **25 y 26 de junio**

**El límite de todas las fechas son a las 23:59 hora en Madrid (CET).**

<h3 class="mt-3 mb-3" id="enviar">Cómo enviar tu propuesta</h3>

El envío de propuestas está abierta a todo el mundo, y aunque seguiremos usando **GitLab** como medio para recoger las propuestas por una cuestión de **transparencia y feedback**, también <u>puedes enviar propuestas mediante los formularios que podrás encontrar en los siguientes enlaces</u>:

-   ̶P̶r̶o̶p̶u̶e̶s̶t̶a̶s̶ ̶d̶e̶ ̶c̶h̶a̶r̶l̶a̶s̶  <span class="red">(enlace desactivado por plazo cerrado)</span>
-   ̶P̶r̶o̶p̶u̶e̶s̶t̶a̶s̶ ̶d̶e̶ ̶t̶a̶l̶l̶e̶r̶e̶s̶  <span class="red">(enlace desactivado por plazo cerrado)</span>
-   ̶P̶r̶o̶p̶u̶e̶s̶t̶a̶s̶ ̶d̶e̶ ̶s̶a̶l̶a̶s̶  <span class="red">(enlace desactivado por plazo cerrado)</span>
-   ̶S̶e̶c̶c̶i̶ó̶n̶ ̶d̶e̶ ̶a̶r̶t̶í̶c̶u̶l̶o̶s̶  <span class="red">(enlace desactivado por plazo cerrado)</span>
-   ̶T̶a̶b̶l̶ó̶n̶ ̶d̶e̶ ̶p̶r̶o̶y̶e̶c̶t̶o̶s̶  <span class="red">(enlace desactivado por plazo cerrado)</span>
-   ̶O̶t̶r̶a̶s̶ ̶a̶c̶t̶i̶v̶i̶d̶a̶d̶e̶s̶  <span class="red">(enlace desactivado por plazo cerrado)</span>

Además, varias de las salas ya aceptadas también tienen abierto su propio envío de propuestas:

-   ̶A̶λ̶h̶a̶m̶b̶r̶a̶ ̶D̶a̶y̶:̶ ̶P̶r̶o̶g̶r̶a̶m̶a̶c̶i̶ó̶n̶ ̶f̶u̶n̶c̶i̶o̶n̶a̶l̶ ̶p̶a̶r̶a̶ ̶t̶o̶d̶o̶ ̶e̶l̶ ̶m̶u̶n̶d̶o̶  <span class="red">(enlace desactivado por plazo cerrado)</span>
-   ̶D̶e̶r̶e̶c̶h̶o̶s̶ ̶D̶i̶g̶i̶t̶a̶l̶e̶s̶ ̶y̶ ̶P̶r̶i̶v̶a̶c̶i̶d̶a̶d̶ ̶e̶n̶ ̶I̶n̶t̶e̶r̶n̶e̶t̶ ̶+̶ ̶S̶o̶b̶e̶r̶a̶n̶í̶a̶ ̶D̶i̶g̶i̶t̶a̶l̶ ̶e̶n̶ ̶l̶a̶s̶ ̶A̶u̶l̶a̶s̶ ̶(̶F̶u̶e̶r̶a̶ ̶G̶o̶o̶g̶l̶e̶)̶  <span class="red">(enlace desactivado por plazo cerrado)</span>
-   ̶D̶e̶s̶a̶r̶r̶o̶l̶l̶o̶ ̶d̶e̶ ̶v̶i̶d̶e̶o̶j̶u̶e̶g̶o̶s̶ ̶c̶o̶n̶ ̶G̶o̶d̶o̶t̶  <span class="red">(enlace desactivado por plazo cerrado)</span>
-   ̶E̶W̶O̶K̶:̶ ̶E̶d̶u̶c̶a̶t̶i̶o̶n̶ ̶W̶i̶t̶h̶ ̶O̶p̶e̶n̶ ̶K̶n̶o̶w̶l̶e̶d̶g̶e̶  <span class="red">(enlace desactivado por plazo cerrado)</span>

Todas las propuestas para <strong>es<span class="red">Libre</span></strong> que se envíen mediante esos formularios quedan registradas en nuestro **[repositorio de propuestas](https://gitlab.com/eslibre/propuestas/-/merge_requests)**, y una vez reciban 3 votos positivos por parte de la organización estará oficialmente aceptada. Si fuera necesario aclarar cualquier cuestión o sugerencia también se haría en ese mismo lugar, por lo que no lo pierdas de vista. En cualquier caso, <u>una vez que registres tu propuesta mediante el formulario, te llegará un correo electrónico con la información de la participación registrada y enlace directo a tu propuesta dentro del repositorio</u>.

Si tienes problemas de cualquier tipo o cualquier tipo de duda, puedes escribirnos a <mailto:propuestas@eslib.re>.

<h3 style="padding-top: 10px; text-align: center;">¡¡¡ANÍMATE A PARTICIPAR!!!<br>👏👏👏</h3>
