---
layout: 2021/default
section: video
title: Sesiones de videoconferencia
permalink: /2021/video/
---

<h1 class="mt-3 mb-3">Sesiones de videoconferencia</h1>

Las sesiones de **pruebas públicas** para **es<span class="red">Libre</span> 2021** han finalizado, así que ya solo queda esperar a los días viernes 25 y sábado 26 de junio para que comience el congreso.

Si tienes cualquier duda, no dudes en escribirnos por correo a [hola@eslib.re](mailto:hola@eslib.re), pero también puedes unirte a nuestro grupo de conversación desde [Matrix](https://matrix.to/#/#esLibre:matrix.org) o [Telegram](https://t.me/esLibre), o escribirnos por redes sociales desde [Mastodon](https://floss.social/@eslibre/) o [Twitter](https://twitter.com/esLibre_).

<h3 style="padding-bottom: 9.3em; text-align: center;">¡¡¡TE ESPERAMOS!!!</h3>
