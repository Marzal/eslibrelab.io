---
layout: 2024/default
section: info
permalink: /2024/info-general/
title: Acerca de esLibre 2024
---

<h1 class="mt-3">
  <strong>es<span class="red">Libre</span></strong> Edición 2024
</h1>

<p align="center">
  <cite style="color: red;">Página con información que se irá actualizando regularmente.</cite>
</p>

**es<span class="red">Libre</span>** es un encuentro de personas interesadas en la divulgación de la cultura libre y las tecnologías libres, tanto en forma de software libre como en forma de hardware libre, así como de las comunidades que hacen esto posible.

Después de la edición más multitudinaria de esLibre hasta la fecha gracias a la colaboración de **[Vitalinux](https://wiki.vitalinux.educa.aragon.es/)**, **[migasfree](http://www.migasfree.org/)** y **[Etopia](https://etopia.es/)** hay que seguir haciendo crecer la comunidad, por lo que esta **6ª edición de es<span class="red">Libre</span>** tendrá como comunidad anfitriona para volver a celebrar estas jornadas de conocimiento libre a **[GNU/Linux València](https://gnulinuxvalencia.org)**, una asociación que desde varios años se dedica a la difusión de todo lo que rodea a **GNU/Linux** desde la *ciudad del Túria* mediante actividades recurrentes como reuniones, talleres, fiestas de instalación poniendo el foco en el software libre en general, la educación y la ética.

Todavía estamos tratando de cerrar algunos detalles con el objetivo de dar más facilidades a la participación y asistencia a esta nueva edición.

### Sobre el congreso

Este año el congreso tendrá la oportunidad de realizarse presencialmente en València gracias a la ya mencionada comunidad anfitriona de **[GNU/Linux València](https://gnulinuxvalencia.org)**, que ha conseguido la cesión de diversos espacios proporcionados por el **[Ajuntament de València](https://www.valencia.es/)** en los edificios de **[Las Naves](https://www.lasnaves.com/)**, el centro de innovación social y urbana de la ciudad de València y **[La Mutant](https://www.lamutant.com/)**, el espacio de artes vivas.

<p class="logo_alg">
<img height="250" alt="Logo GNU/Linux València" src="/2024/assets/logos/gnulinuxvalencia.png">
</p>

Se seguirá el mismo formato que en años anteriores: charlas, talleres, mesas de comunidades y exposición, salas organizadas por comunidades… contando con el apoyo de colaboradores como **[Interferencias](https://interferencias.tech/)**, **[LibreLabGRX](https://librelabgrx.cc/)**, **[GALPon](https://www.galpon.org/)**, **[OfiLibre de la URJC](https://ofilibre.gitlab.io/)**, **[Wikimedia España](https://www.wikimedia.es/)**, **[Slimbook](https://slimbook.com/)**, **[Librebit](https://www.librebit.com/)** o **[Aitire](https://www.aitire.cloud/)** que ayudarán en la organización y sumarán esfuerzos en la realización de esta edición en formato híbrido presencial/virtual.

El <strong>envío de propuestas está oficialmente abierto</strong> en una <strong>primera fase hasta el 28 de enero</strong> y una <strong>segunda fase hasta el 31 de marzo</strong>, así que puedes informarte sobre cómo proponer actividades para el congreso en la página de <a href="/2024/propuestas/"><strong>envío de propuestas</strong></a>.

### Cómo llegar

Aunque la sede de este año está compuesta por dos edificios, ambos anexos y están conectados físicamente de forma que se puede pasar de uno a otro directamente:

* **[Las Naves](https://www.lasnaves.com/)**, es el centro de innovación social y urbana de la ciudad de València, formalmente una entidad del sector público local adscrita a la **Delegació d’Innovació i Gestió del Coneixement** del **[Ajuntament de València](https://www.valencia.es/)**, cuya misión está definida como *"aprovechar todas las oportunidades del conocimiento, de la creatividad, del talento, de la ciencia, de la investigación, de la tecnología, de las humanidades, etc. que nos ofrece la vida y, mediante la innovación 'circular', que empieza y acaba en las personas, canalizarlas para devolver a la ciudadanía soluciones innovadoras que mejoren sus vidas"*.

* **[La Mutant](https://www.lamutant.com/)**, *Espai d'Arts Vives*, es un proyecto con vocación de servicio público y accesibilidad universal que el **[Ajuntament de València](https://www.valencia.es/)** gestiona a través de la **Regidoria d’Acció Cultural**: *"En una ciudad inmersa en plena eclosión creativa, llena de proyectos e iniciativas públicas, privadas y cívicas con denominador cultural, el espacio viene a completar una oferta escénica, que, por un lado, se adapte a la evolución creativa de los nuevos lenguajes de exhibición y que a la vez atienda una diversidad de públicos que demandan a la administración una apuesta clara por el riesgo"*.

Situados cerca de la costa y el Puerto de València, se encuentra a unos 30 minutos de la [estación de tren València Estació del Nord](https://www.adif.es/-/65000-val%C3%A8ncia-est.-nord) y la [Estación Central de Autobuses de Valencia](https://www.valencia.es/es/-/infociudad-estaci%C3%93n-de-autobuses-de-valencia) y a una 1 hora del [Aeropuerto de Valencia](https://www.aena.es/es/valencia.html), por lo que hay diferentes opciones para asistir a la edición de este año por carretera, tren o avión.

<p style="padding-bottom: 1em;" class="logo_alg">
<img style="margin-right: 2em" width="550px" src="/2024/assets/imgs/naves_01.jpg">
<img width="550px" src="/2024/assets/imgs/naves_02.jpg">
</p>

<p style="padding-bottom: 2em;" class="logo_alg">
<img style="margin-right: 2em" width="550px" src="/2024/assets/imgs/mutant_01.jpg">
<img width="550px" src="/2024/assets/imgs/mutant_02.jpg">
</p>

<a href="https://www.lasnaves.com/" target="_blank"><img src="/2024/assets/logos/lasnavesmutant.png" alt="Las Naves" style="display: block; margin-left: auto; margin-right: auto;" class="images-small"></a>

<iframe style="padding-top: 2em !important" width="100%" height="300px" frameborder="0" allowfullscreen src="https://www.openstreetmap.org/export/embed.html?bbox=-0.338905155658722%2C39.4577628545084%2C-0.33639460802078247%2C39.45913378709921&amp;layer=mapnik&amp;marker=39.45844831917188%2C-0.3376498818397522"></iframe><p><a href="https://www.openstreetmap.org/?mlat=39.45845&mlon=-0.33765#map=19/39.45845/-0.33765" target="_blank">Ver pantalla completa</a></p>

<p style="text-align: center;">Si tienes cualquier duda, no dudes en escribirnos a <a href="mailto:hola@eslib.re">hola@eslib.re</a>.</p>

<h3 style="padding-top: 10px; text-align: center;">¡¡¡NOS VEMOS DENTRO DE POCO!!!<br>👏👏👏</h3>
